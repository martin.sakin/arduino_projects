# Arduino_projects

Author: Martin Sakin

Random projects. This git includes some my experimentation with Arduino.


## led_strip_hc_sr501_160227

Automatic switching on and off the LED strip using the Arduino Mini.


## ledstrip_kitchen_20200818

Automatic switching on and off the LED strip using the Arduino Nano.


## traffic_light_control

Read data about CO2 from MQTT in Python3 and control Arduino Nano with semaphore.


