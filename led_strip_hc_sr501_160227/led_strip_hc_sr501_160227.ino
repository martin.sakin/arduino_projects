/**
 * Control white LED strip by Arduino.
 * Author: Martin Sakin
 * Date: 2016-02-22
 */

const int sensor = 6;    // the number of the pushbutton pin
const int stripPin = 3;  // must be PWM-capable pins!
const int infoLed = 13;    // LED on the Arduino board (L)
const int max_white = 255;  // maximum intensity
const int time_wait = 60;  // X secunds stay light after last move
int saveval = 0;    // stored value of intensity

void setup() {
  // initialize inputs:
  pinMode(sensor, INPUT);  // hc_sr501
  
  // initialize outputs:
  pinMode(infoLed, OUTPUT);    // info LED
  pinMode(stripPin, OUTPUT);  // led strip
 
  //Serial.begin(9600); 
}

void loop(){
  if (digitalRead(sensor) == HIGH) { // turn ON
    digitalWrite(infoLed, HIGH);
    for(int i=saveval; i < max_white; i++){
      //Serial.print("intensity = ");
      //Serial.println(i);
      analogWrite(stripPin, i);
      saveval = i;
      delay(20);  // continuous illumination
    }
    for(int j=0; j < time_wait; j++){
      //Serial.println(j);
      delay(1000);
      if (digitalRead(sensor) == HIGH) { break; }
    }
  }
  else { // turn OFF
    digitalWrite(infoLed, LOW);
    for(int i=saveval; i > -1; i--){
      if (digitalRead(sensor) == HIGH) { break; }
      //Serial.print("intensity = ");
      //Serial.println(i);
      analogWrite(stripPin, i);
      saveval = i;
      delay(40);  // stepless dimming
    }
  }
}
