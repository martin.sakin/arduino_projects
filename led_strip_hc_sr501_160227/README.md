# Project: led_strip_hc_sr501_160227
# Author: Martin Sakin
# Date: 02-2016

Automatic switching on and off the LED strip using the Arduino Mini.

Use:
When somebody enter to my kitchen, kitchen desk itself pleasantly light up automatically.
After one minute without any movement, slowly begins to dim.

I have used Arduino for easy setting of optimal times using code.
And because I'm IT guy and not an electrician :)

I used:
- Arduino Pro Mini 328 ATMEGA328 5V/16MHz
- HC-SR501 PIR MOTION DETECTOR
- Resistor 1kΩ
- Bipolar transistor TIP31 TO220 BS239c
- Solder board
- White LED strip 12V
- Power supply 12V 1A
