# Project: ledstrip_kitchen_20200818

Author: Martin Sakin

Date: 08-2020

Description: Automatic switching on and off the LED strip using the Arduino Nano.

Use: When somebody enter to kitchen, kitchen desk itself pleasantly light up automatically.
After one minute without any movement, slowly begins to dim.


## Components:
 - Board: Arduino Nano, Processor: ATmega328P ((new))
 - HC-SR501 PIR MOTION DETECTOR
 - Resistor 1kΩ
 - Unipolární tranzistor IRL530N TO220AB
 - Solder board
 - White LED strip 12V
 - Power supply 12V
