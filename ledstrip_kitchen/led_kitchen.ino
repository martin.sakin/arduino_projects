/**
   Control white LED strip by Arduino.
   Author: Martin Sakin
   Date: 2020-08-18
*/

const short DEBUG = 0;
const int sensor = 9;    // the number of hc_sr501 pin
const int stripPin = 3;  // must be PWM-capable pins!
const int infoLed = 13;    // LED on the Arduino board (L)
const int max_white = 255;  // maximum intensity
const int time_wait = 90;  // X secunds stay light after last move
int saveval = 0;    // stored value of intensity

void setup() {
  // initialize inputs:
  pinMode(sensor, INPUT);  // hc_sr501

  // initialize outputs:
  pinMode(infoLed, OUTPUT);    // info LED
  pinMode(stripPin, OUTPUT);  // led strip

  if (DEBUG) Serial.begin(9600);
}

void loop() {
  if (digitalRead(sensor) == HIGH) { // turn ON
    digitalWrite(infoLed, HIGH);
    for (int i = saveval; i < max_white; i++) {
      if (DEBUG) Serial.print("intensity UP = ");
      if (DEBUG) Serial.println(i);
      analogWrite(stripPin, i);
      saveval = i;
      delay(20);  // continuous illumination
    }
    for (int j = 0; j < time_wait; j++) {
      delay(1000);
      if (digitalRead(sensor) == HIGH) {
        break;
      }
    }
  }
  else { // turn OFF
    digitalWrite(infoLed, LOW);
    int beforeDark = 80;
    for (int i = saveval; i >= 0; i--) {
      if (digitalRead(sensor) == HIGH) {
        break;
      }
      if (DEBUG) Serial.print("intensity DOWN = ");
      if (DEBUG) Serial.println(i);
      analogWrite(stripPin, i);
      saveval = i;
      if (i == 4) {
        if (beforeDark > 0) {
          beforeDark--;
          i++;
        }
      }
      delay(40);  // stepless dimming
    }
  }
}
