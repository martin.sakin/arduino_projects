# Traffic light control

Read data about CO2 from MQTT in Python3 and control Arduino Nano with semaphore.

Python script runs as service on RPi (or some PC).
To this RPi/PC is connected Arduino Nano by USB with traffic lights.
Python script reads data about CO2 state and by the level of ppm set color on traffic lights.
