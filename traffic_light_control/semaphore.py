#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Date : 2019-03-27
# @Author : Martin Sakin, martin.sakin <at> gmail.com
# Control Arduino by USB, read data (CO2) from MQTT
#  and turn on the light on the semaphore by value.

"""
git clone https://github.com/nanpy/nanpy
cd nanpy
py3 setup.py install
cd ~
mkdir ard_ctrl
cd ard_ctrl
vim semaphore.py
"""

from nanpy import ArduinoApi, SerialManager
import time
import paho.mqtt.client as paho
import json

RED = 2
YELLOW = 3
GREEN = 4

MQTT_BROKER = "test.mosquitto.org"
MQTT_TOPIC = "123456789123456"
DEVICE_ID = "0xa90081111cdd15af"

class Ard():
	def __init__(self):
		pass

	def connect(self):
		try:
			connection = SerialManager()
			self.ard = ArduinoApi(connection = connection)
		except:
			print("connection FAILED")
			exit(1)

		self.ard.pinMode(RED, self.ard.OUTPUT)
		self.ard.pinMode(YELLOW, self.ard.OUTPUT)
		self.ard.pinMode(GREEN, self.ard.OUTPUT)

		self.test()

	def test(self):
		delay = 0.1
		for i in [RED, YELLOW, GREEN]:
			self.on(i)
			time.sleep(delay)

		time.sleep(0.5)

		for i in [RED, YELLOW, GREEN]:
			self.off(i)
			time.sleep(delay)

	def on(self, color):
		self.ard.digitalWrite(color, self.ard.HIGH)

	def off(self, color):
		self.ard.digitalWrite(color, self.ard.LOW)


class MqttReader:
	def __init__(self):
		self.broker = MQTT_BROKER
		self.topic = MQTT_TOPIC
		self.device = DEVICE_ID
		self.module = 2
		self.connected = False
		self.client = paho.Client()
		self.client.on_connect = self.on_connect_reader
		self.client.on_message = self.on_message

		self.client.connect(self.broker)
		self.client.loop_start()

		self.ard = Ard()
		self.ard.connect()

	def on_connect_reader(self, client, userdata, flags, rc):
		if rc == 0:
			self.connected = True
		else:
			print("READER cannot connect to:", self.broker)
			exit(2)

	def on_message(self, client, userdata, message):
		msg = str(message.payload.decode("utf-8"))
		json_msg = json.loads(msg)
		if json_msg['device_id'] == self.device:
			for mod in json_msg['data']:
				if mod["module_id"] == self.module:
					value = mod["value"]
					print(time.ctime()," value =", value, "ppm")
					self.evaluate(value)

	def evaluate(self, value):
		if value >= 1000:
			self.ard.on(RED)
			self.ard.off(YELLOW)
			self.ard.off(GREEN)
		elif value >= 650 and value < 1000:
			self.ard.off(RED)
			self.ard.on(YELLOW)
			self.ard.off(GREEN)
		elif value < 650:
			self.ard.off(RED)
			self.ard.off(YELLOW)
			self.ard.on(GREEN)


	def loop(self):
		while not self.connected:
			time.sleep(0.1)

		self.client.subscribe(self.topic)

		while True:
			time.sleep(10)

	def close(self):
		self.client.loop_stop()
		self.client.disconnect()


def main():
	while True:
		reader = MqttReader()
		try:
			reader.loop()
		except KeyboardInterrupt:
			print(" EXIT by user")
		finally:
			reader.close()

if __name__ == '__main__':
	main()
